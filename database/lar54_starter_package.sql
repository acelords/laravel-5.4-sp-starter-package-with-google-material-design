-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2017 at 04:24 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lar54_starter_package`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `folder` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `folder`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'education', 'Education', 'Apps for Educational Purposes', '2017-03-10 20:56:15', '2017-03-10 20:56:15'),
(2, 'business', 'Business & Finance', 'Apps for Business Purposes', '2017-03-10 20:56:15', '2017-03-10 20:56:15'),
(3, 'books', 'Books', 'Apps for Reading Purposes', '2017-03-10 20:56:15', '2017-03-10 20:56:15'),
(4, 'entertainment', 'Entertainment', 'Apps for Entertainment Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(5, 'health', 'Health', 'Apps for Heealth Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(6, 'lifestyle', 'Lifestyle', 'Apps for Lifestyle Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(7, 'social', 'Social', 'Apps for Social Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(8, 'sports', 'Sports', 'Apps for Sporting Activities Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(9, 'travel', 'Travel', 'Apps for travelling Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(10, 'wallpaper', 'Wallpaper & Themes', 'Apps for Wallpaper Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(11, 'utilities', 'Utilities', 'Apps for Utility Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(12, 'languages', 'Languages & Translators', 'Apps for Languages Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(13, 'multimedia', 'Multimedia', 'Apps for Multimedia Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(14, 'productivity', 'Productivity', 'Apps for Productivity Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(15, 'games', 'Games', 'Apps for Gaming Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16'),
(16, 'communication', 'Communication', 'Apps for Communication Purposes', '2017-03-10 20:56:16', '2017-03-10 20:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_10_220721_create_site_details_table', 2),
(8, '2017_03_10_231347_create_products_table', 3),
(9, '2017_03_10_231543_create_categories_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `downloads` int(11) NOT NULL,
  `appname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `category_id`, `downloads`, `appname`, `description`, `device_type`, `icon`, `img1`, `img2`, `img3`, `filename`, `extension`, `created_at`, `updated_at`) VALUES
(1, 8, 14, 30, 'Beautiful People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57'),
(2, 8, 2, 20, 'Moon People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57'),
(3, 8, 4, 20, 'Bare People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57'),
(4, 8, 5, 56, 'Nude People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57'),
(5, 8, 2, 576, 'Couch People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57'),
(6, 8, 8, 67, 'Bed People', '<p>Enter the description here</p>', 'Android 5.1', 'BeautifulPeople-icon-1488440276.png', 'BeautifulPeople-screenshot1-1488440276.jpg', 'BeautifulPeople-screenshot2-1488440276.jpg', 'BeautifulPeople-screenshot3-1488440276.jpg', 'BeautifulPeople1488440276.jar', 'apk', '2017-03-02 04:37:57', '2017-03-02 04:37:57');

-- --------------------------------------------------------

--
-- Table structure for table `site_details`
--

CREATE TABLE `site_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_details`
--

INSERT INTO `site_details` (`id`, `option_name`, `option_value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'My Website', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(2, 'site_url', 'Http://www.mywebsite.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(3, 'site_slogan', 'The best website in town!!', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(4, 'site_desc', 'This is the best website ever based on Laravel 5, a PHP web framework for web artisans. Very expressive and intuitive!', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(5, 'site_bio', '<p>Starting out in the year 2017, we have been devoted to our sole mission of providing the best website experiences in Kenya and round Africa.</p>', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(6, 'site_address', 'Nairobi, Kenya', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(7, 'site_phone', '254701234567', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(8, 'site_email', 'info@mywebsite.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(9, 'site_fax', '29856', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(10, 'site_logo', 'main-logo-1489216522.png', '2017-03-10 19:11:51', '2017-03-11 04:15:22'),
(11, 'site_version', 'v1.1 "RedStone 1"', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(12, 'site_facebook', 'facebook.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(13, 'site_twitter', 'twitter.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(14, 'site_googleplus', 'googleplus', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(15, 'site_youtube', 'youtube.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51'),
(16, 'site_instagram', 'instagram.com', '2017-03-10 19:11:51', '2017-03-10 19:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `othernames` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `status`, `surname`, `fname`, `othernames`, `email`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Main', 'Admin', 'Yule Mzii', 'admin@gmail.com', '$2y$10$3Q1S.CZ9sCQDxOPMFk8LXuSR7aawXrt9cvx.WerPo7fcKJdcD4HwS', 'avatar.png', NULL, '2017-03-10 19:11:50', '2017-03-10 19:11:50'),
(2, 1, 'YungCarter', 'Lexx', '', 'lexx@acelords.space', '$2y$10$YGmc8HyAIl3q/R9vUmryUOWMu9o3/Bz.K5qW/TuXpo9Ub6xl5PxSm', 'avatar.png', NULL, '2017-03-10 20:34:12', '2017-03-10 20:34:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_details`
--
ALTER TABLE `site_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `site_details_option_name_unique` (`option_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `site_details`
--
ALTER TABLE `site_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
