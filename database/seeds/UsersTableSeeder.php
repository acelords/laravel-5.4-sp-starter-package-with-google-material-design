<?php

use Illuminate\Database\Seeder;
use App\User; // the model

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->status = 1;
        $user->fname = "Admin";
        $user->surname = "Main";
        $user->othernames = "Yule Mzii";
        $user->email = "admin@gmail.com";
        $user->password = bcrypt('admin123');
        $user->avatar = "avatar.png";
        $user->save();        
        
        
        // $user = new User();
        // $user->status = 1;
        // $user->fname = "Lexx";
        // $user->surname = "YungCarter";
        // $user->othernames = "";
        // $user->email = "lexx@acelords.space";
        // $user->password = bcrypt('*lexx***');
        // $user->avatar = "avatar.png";
        // $user->save();        
        
    }
}
