<?php

use Illuminate\Database\Seeder;
use App\SiteDetail; //the model

class SiteDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = new SiteDetail();
        $name->option_name = 'site_name';
        $name->option_value = 'My Website';
        $name->save();

        $Url = new SiteDetail();
        $Url->option_name = 'site_url';
        $Url->option_value = 'Http://www.mywebsite.com';
        $Url->save();

        $slogan = new SiteDetail();
        $slogan->option_name = 'site_slogan';
        $slogan->option_value = 'The best website in town!!';
        $slogan->save();

        $slogan = new SiteDetail();
        $slogan->option_name = 'site_desc';
        $slogan->option_value = 'This is the best website ever based on Laravel 5, a PHP web framework for web artisans. Very expressive and intuitive!';
        $slogan->save();

        $bio = new SiteDetail();
        $bio->option_name = 'site_bio';
        $bio->option_value = '<p>Starting out in the year 2017, we have been devoted to our sole mission of providing the best website experiences in Kenya and round Africa.</p>';
        $bio->save();

        $add = new SiteDetail();
        $add->option_name = 'site_address';
        $add->option_value = 'Nairobi, Kenya';
        $add->save();

        $phone = new SiteDetail();
        $phone->option_name = 'site_phone';
        $phone->option_value = '254701234567';
        $phone->save();

        $email = new SiteDetail();
        $email->option_name = 'site_email';
        $email->option_value = 'info@mywebsite.com';
        $email->save();

        $fax = new SiteDetail();
        $fax->option_name = 'site_fax';
        $fax->option_value = '29856';
        $fax->save();

        $logo = new SiteDetail();
        $logo->option_name = 'site_logo';
        $logo->option_value = 'logo.png';
        $logo->save();

        
        $logo = new SiteDetail();
        $logo->option_name = 'site_version';
        $logo->option_value = 'v1.1 "RedStone 1"';
        $logo->save();
        
        $logo = new SiteDetail();
        $logo->option_name = 'site_facebook';
        $logo->option_value = 'facebook.com';
        $logo->save();

        $logo = new SiteDetail();
        $logo->option_name = 'site_twitter';
        $logo->option_value = 'twitter.com';
        $logo->save();

        $logo = new SiteDetail();
        $logo->option_name = 'site_googleplus';
        $logo->option_value = 'googleplus';
        $logo->save();

        $logo = new SiteDetail();
        $logo->option_name = 'site_youtube';
        $logo->option_value = 'youtube.com';
        $logo->save();

        $logo = new SiteDetail();
        $logo->option_name = 'site_instagram';
        $logo->option_value = 'instagram.com';
        $logo->save();

    }
}
