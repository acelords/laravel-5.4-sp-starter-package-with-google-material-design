<?php

use Illuminate\Database\Seeder;

use App\Product; // the model

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $images = array("android.jpg", "windows2.jpg", "android.jpg", "img3yes.jpg");
        // $images = json_encode($images);

        $appnames = array(
            "Flashlight", "City run", "Hill Climb Racing", "Temple Run 2",
            "Bubble shooter", "Opera Mini", "Candy Crush Saga", "Vegas climb",
            "Ultimate soccer", "Taxi Driver 3D"
        );
        $device_type = array("android","blackberry","ios","html 5","java","symbian","windows phone","windows mobile");

        for($i=0; $i < 10; $i++) {
            $rand1 = rand(1,10); // categories
            $rand2 = rand(100,5500); // downloads
            $rand3 = rand(0,7); // downloads
            $rand3 = rand(0,2); // images

            $product = new Product();
            $product->user_id = 9;
            $product->category_id = $rand1;
            $product->downloads = $rand2;
            $product->appname = $appnames[$i];
            $product->description = "best " . $appnames[$i] . " ever";
            $product->device_type = $device_type[$rand3];
            $product->icon = $appnames[$i] . ".jpg";
            $product->img1 = $images[$rand3];
            $product->img2 = $images[($rand3 + 1)];
            $product->img3 = $images[$rand3];
            $product->filename = $appnames[$i] . ".apk";
            $product->extension = ".apk";
            $product->save();
        }
    }
}
