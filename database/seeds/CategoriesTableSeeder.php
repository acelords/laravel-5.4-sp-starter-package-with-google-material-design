<?php

use Illuminate\Database\Seeder;
use App\Category; // the mdoel

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = new Category();
        $cat->name = "Education";
        $cat->folder = "education";
        $cat->description = "Apps for Educational Purposes";
        $cat->save();
        
        $cat = new Category();
        $cat->name = "Business & Finance";
        $cat->folder = "business";
        $cat->description = "Apps for Business Purposes";
        $cat->save();
        
        $cat = new Category();
        $cat->name = "Books";
        $cat->folder = "books";
        $cat->description = "Apps for Reading Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Entertainment";
        $cat->folder = "entertainment";
        $cat->description = "Apps for Entertainment Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Health";
        $cat->folder = "health";
        $cat->description = "Apps for Heealth Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Lifestyle";
        $cat->folder = "lifestyle";
        $cat->description = "Apps for Lifestyle Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Social";
        $cat->folder = "social";
        $cat->description = "Apps for Social Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Sports";
        $cat->folder = "sports";
        $cat->description = "Apps for Sporting Activities Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Travel";
        $cat->folder = "travel";
        $cat->description = "Apps for travelling Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Wallpaper & Themes";
        $cat->folder = "wallpaper";
        $cat->description = "Apps for Wallpaper Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Utilities";
        $cat->folder = "utilities";
        $cat->description = "Apps for Utility Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Languages & Translators";
        $cat->folder = "languages";
        $cat->description = "Apps for Languages Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Multimedia";
        $cat->folder = "multimedia";
        $cat->description = "Apps for Multimedia Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Productivity";
        $cat->folder = "productivity";
        $cat->description = "Apps for Productivity Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Games";
        $cat->folder = "games";
        $cat->description = "Apps for Gaming Purposes";
        $cat->save();

        $cat = new Category();
        $cat->name = "Communication";
        $cat->folder = "communication";
        $cat->description = "Apps for Communication Purposes";
        $cat->save();
    }
}
