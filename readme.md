<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About This Project ; Lar 5.4

Laravel is a web application framework with expressive, elegant syntax. Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

It is therefore of utmost importance if one was to have a ready-to-go-and-deploy Laravel 5 template, and this is exactly what this project aims to accomplish. Simply put, the common files needed for a quick deployment are included, vendor packages, and configurations (migrations and Seeder Files also available). All you need to do is run 'composer update' on your development project folder, followed by 'php artisan key:generate' (in case you would like a new key), followed by 'php artisan make:migration --seed' and you are all set to go.

## Why This Project Will Be the Cornerstone of all Your Upcoming Projects

Everyone hates re-writing code they have painstakingly written in the past. If you don't, well, you are just lying or not doing as many projects as your potential allows you to!

There are code samples and best workarounds included in the project. First of all, the AppServiceProvider is where the 'Site Details' are 'booted', meaning the site details has been shared to all views. the 'site_details' table contains all the site_details configurations, similar to Wordpress' style of 'Wp-Options' table.

2nd, the ajax login is shown, and what is only required is copying the ajax-login.js to the js folder in the public folder ('/js/). The script file can be customized to your liking.

3rd, the admin panel has the most disturbing yet simple php-javascript integrations. There are sample works of chart.js implementations, datatables (Laravel powered to handle billions of data via ajax loading), and a nice bootstrap-based Google Material Design oriented dashboard panel. An intuitive dateTimePicker is integrated by default with 3 options; a date-only picker, a time-only picker, and a combined date-and-time picker. they are pre-initialized.

4th, the controllers have basic and common function/algorithms/coding practices that you can just copy and paste. Straight from uploading documents (with MIMe type detection), to downloading files with the correct headers, all you need to do is copy-and-paste, and customize to your needs.

## Contributing

You are hereby welcome to continue in developing this templates and more others with ready-made themes and scripts. Let's continue adding fun to Laravel!

## Security Vulnerabilities

If you discover a security vulnerability within this Laravel project, please send an e-mail to Lexx YungCarter at lexx@acelords.space. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT). However, this one ain't! Taylor Otwel is driving, we are still boarding matatus. So till then, this will be termed as an enterprise level template for all parties involved in it's design and maintenance. Long story short, it is free for use by this party in doing projects, but not to be given free-of-charge to other non-authorised parties.
