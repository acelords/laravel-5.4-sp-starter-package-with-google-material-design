$(document).ready(function () {
    $('.onlydatepicker').datetimepicker({
        lang: 'en',
        timepicker: false,
        format: 'd/m/Y',
        formatDate: 'Y/m/d',
        // minDate:'-1970/01/02', // yesterday is minimum date
        minDate: '-1970/01/01', // today is minimum date
        // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
    });

    $('.onlytimepicker').datetimepicker({
        lang: 'en',
        datepicker: false,
        format: 'H:i',
        step: 30
    });

});

// ajax login
jQuery(document).ready(function() {

	jQuery(document).ajaxStart(function () {
	    jQuery('#loginform input[type=submit]').val('Loading...');
    	jQuery('#loginform input[type=submit]').css('background', '#FFA579');
	}).ajaxStop(function () {
		
	});

	jQuery('#loginform').submit(function(b) {
		b.preventDefault();
		var email=jQuery("#loginform #email").val();
		var password=jQuery("#loginform #password").val();

	    if(jQuery('#loginform #username').hasClass('has-error')){
	        jQuery('#loginform #username').removeClass('has-error');
	    }

	    if(jQuery('#loginform #password').hasClass('has-error')){
	        jQuery('#loginform #password').removeClass('has-error');
	    }

	    var form = jQuery("#loginform");
	    var dataString = form.serialize();
	    var formAction = form.attr('action');

	    jQuery.ajax({
	        type: "POST",
	        url : formAction,
	        data : dataString,
	        success : function(data){
	            console.log(data.auth);

	            if (data.auth == true) {
		            setTimeout(
		                function()
		                {
		                    jQuery('#loginform input[type=submit]').val('Login Success');
		                    jQuery('#loginform input[type=submit]').css('background', '#449d44');

		                    setTimeout(
		                        function()
		                        {
		                            window.location.href = data.intended;

		                        }, 2000);

		                }, 2000);

	            } else {
        	    	jQuery('#loginform input[type=submit]').val('Invalid! Try Again');
        	    	jQuery('#loginform input[type=submit]').css('background', '#F2DEDE');
	            	setTimeout(
	            	    function()
	            	    {
		        	    	jQuery('#loginform input[type=submit]').val('Login Again');
		        	    	jQuery('#loginform input[type=submit]').css('background', '#67B7E1');
	            	    }, 5000);
	            }


	        },
	        error : function(data){
	            var errors = jQuery.parseJSON(data.responseText);
	            console.log(errors);

	            // setTimeout(
	            //     function()
	            //     {

	            //         errorsHtml = '<div class="alert alert-danger text-center"><h4 class="margin-top-20" style="font-size: 18px;"><i class="fa fa-exclamation"></i> Anerror occurred</h4><p style="color:#35393b">Please check your details again</p><ul class="list-unstyled">';
	            //         jQuery.each( errors , function( key, value ) {
	            //             errorsHtml += '<li style="color:#35393b;">' + value[0] + '</li>'; //showing only the first error.
	            //         });
	            //         errorsHtml += '</ul></di>';

	            //         jQuery( '#form-errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form

	            //         if(errors.email){
	            //             jQuery('#loginform #email').addClass('has-error');
	            //             jQuery('#loginform #email i').css('color', '#a94442');
	            //         }

	            //         if(errors.email){
	            //             jQuery('#loginform #password').addClass('has-error');
	            //             jQuery('#loginform #password i').css('color', '#a94442');
	            //         }

	            //         jQuery('#loginform input[type=submit]').val('Reset');

	            //     }, 1500);

	        }

	    },"json");
	});

});