/*
Customization Script by: Lexx YungCarter
Github: Lexx-YungCarter
Twitter: @UnderscoreLexx
Google+: google.com/+LexxYungCarter
==========================
This script is beautifully made with passion based on the modern approaches of JavaScript
Technologies. The items outlined here pose some critical UI and UX design patterns that should 
not be edited (unless of course, under supervision or parental guidance :-D).
Feel free to copy and paste to your projects, but on condition that you credit the author 
(of course Me, daah!)

***HAPPY CODING***

*/

// Setting up custom Laravel Notices, based on messages passed from controllers
// PNotify is beautiful, and well, Free!
var successModalTrigger = false;
var infoModalTrigger = false;
var warningModalTrigger = false;
var failModalTrigger = false;

var notice = false;

// PNotify.prototype.options.styling = "bootstrap3"; // uses only bootstrap
PNotify.prototype.options.styling = "fontawesome"; // uses bootstrap and font-awesome

successModalTrigger = $('#successModalTrigger').data('content');
infoModalTrigger = $('#infoModalTrigger').data('content');
warningModalTrigger = $('#warningModalTrigger').data('content');
failModalTrigger = $('#failModalTrigger').data('content');

// success notify
if (successModalTrigger) {
    notice = new PNotify({
        title: 'Success!',
        text: successModalTrigger,
        icon: 'ion-android-checkmark-circle',
        type: 'success'
    });
} 

// info notify
if (infoModalTrigger) {
    notice = new PNotify({
        title: 'Info!',
        text: infoModalTrigger,
        icon: 'ion-alert',
        type: 'info'        
    });

}

// warning notify
if (warningModalTrigger) {
    notice = new PNotify({
        title: 'Warning!',
        text: warningModalTrigger,
        icon: 'ion-android-alert'
    });

}

// error notify
if (failModalTrigger) {
    notice = new PNotify({
        title: 'Error!!',
        text: failModalTrigger,
        icon: 'ion-android-cancel',
        type: 'error'
    });
}

// check if notice is true
if (notice) {
    notice.get().click(function() {
        notice.remove();
    });
}

// lexx preloader 
$(document).ready(function () {
    
    // initialize datetime picker
    $('.datetimepicker').datetimepicker({
        // timepicker:false,
        // format:'Y-m-d',
        // formatDate:'Y-m-d'
    });
    
    $('.onlydatepicker').datetimepicker({
        lang: 'en',
        timepicker: false,
        format: 'd/m/Y',
        formatDate: 'Y/m/d',
        // minDate:'-1970/01/02', // yesterday is minimum date
        minDate: '-1970/01/01', // today is minimum date
        // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
    });

    $('.onlytimepicker').datetimepicker({
        lang: 'en',
        datepicker: false,
        format: 'H:i',
        step: 30
    });
});


function lexx_date_formatter(the_date) {
    if (the_date == "") {
        return null;
    }
    var m_names = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    var formattedDate = new Date(the_date);
    var d = formattedDate.getDate();
    var m = formattedDate.getMonth();
    // m += 1; // JS are 0-11
    // no need to increment months when displaying month names
    var yr = formattedDate.getFullYear();
    return (d + " " + m_names[m] + ", " + yr);
}
    
// get item name
function get_item_name(items_array, item_id) {
    var item_name = null;
    // iterate through the array/object
    $.each(items_array, function(index, value) {
        if(index == parseInt(item_id)) {
            // console.log(value);
            item_name = value;
        }
    }); 
    //end of iteration
    return item_name;
}