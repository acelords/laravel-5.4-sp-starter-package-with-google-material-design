<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
*   Custom Login Routes
*   Customized to work with laravel 5.4 new auth routes
*/
// uncomment this to enable all auth routes, including Registration
// Auth::routes(); 
Route::post('login', ['as'=>'login', 'uses'=>'Auth\LoginController@login']);
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
// ajax login
Route::post('login/ajax', ['as'=>'ajax-login', 'uses'=>'Auth\LoginController@postLogin']);


/**
*   ===========================================================
*   Frontend Routes
*   ===========================================================
*/
Route::get('/', ['as'=>'home', 'uses'=>'HomeController@index']);
Route::get('/home', 'HomeController@index');

/**
*   ===========================================================
*   Backend Routes - Prefix 'admin'
*   ===========================================================
*/
Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function() {

    Route::get('/', ['as'=>'admin.dashboard', 'uses'=>'AdminController@index']);
    
    /**
    * ========================================================
    * Products Area 
    * ========================================================
    */
    Route::get('/datatables/', ['as'=>'admin.datatables', 'uses'=>'AdminController@datatables']);
    Route::get('/products/datatables/show', ['as'=>'view-product', 'uses'=>'AdminController@datatables']);
    Route::get('/products/datatables/edit', ['as'=>'admin.edit-product', 'uses'=>'AdminController@datatables']);
    Route::get('/products/datatables/destroy', ['as'=>'admin.delete-product', 'uses'=>'AdminController@datatables']);

    /**
    * ========================================================
    * Datatables Area 
    * ========================================================
    */
    Route::get('/products/ajax/fetch/', ['as'=>'admin.fetch-products-ajax', 'uses'=>'DatatablesController@productsData']);

    /**
    * ========================================================
    * Site Settings Area 
    * For use even with dropzone.js
    * ========================================================
    */
    
    Route::get('/settings', ['as'=>'admin.settings', 'uses'=>'AdminController@settings']);
    Route::put('/settings/', ['as'=>'admin.update-settings','uses'=>'AdminController@settingsUpdate']);

    // site logos (dropzone.js)
    Route::get('/settings/mainlogo', ['as'=>'admin.main-logo', 'uses'=>'AdminController@mainLogo']);
    Route::post('/settings/updateLogo', ['as'=>'admin.update-logo', 'uses'=>'AdminController@updateLogo']);

    // fetch user avatars stored in storage/apps/public/img/users folder
    Route::get('/avatar/{avatar}', function($avatar) {
        
        // return Image::make(storage_path() . '/app/public/img/users/' . $avatar)->response();
        $path = storage_path() . '/app/public/img/users/' . $avatar;

        if(!File::exists($path)) { return ''; }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    })->name('user-avatar');

});
