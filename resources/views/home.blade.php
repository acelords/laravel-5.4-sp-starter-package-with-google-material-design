@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="alert alert-info">
                        <h4>Hi there. You can use this sample Ajax form to login Users.</h4>
                        <p>The script file is located at <strong>/js/ajax-login.js</strong></p>
                    </div>
                    {{ Form::open(['class'=>'form', 'id'=>'loginform', 'route'=>'ajax-login', 'method'=>'post']) }}
	                    <input type="email" name="email" id="email" value="" size="20" placeholder="Email" required>
	                    <input type="password" name="password" id="user_pass" value="" placeholder="Password" required>
	                    <input type="submit" name="submit" id="submit" value="Log In" class="btn btn-primary">

						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
							</label>
						</div>
	                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
