@extends('admin.layouts.main')
@section('styles')
    {{ Html::style('/backend/css/dropzone.min.css') }}
@stop

@section('content')
    <div class="panel panel-flatborder-left-lg border-left-info">
          <div class="panel-heading">
             <h6 class="panel-title text-bold">Tweak the Site's Settings</h6>
             <div class="heading-elements">
                 <span class="heading-text"><i class="icon-clock fg-purple"></i> {{ date('j F, Y') }} </span>
             </div>
          </div>
          <div class="panel-body">
            {{ Form::open(['class'=>' form-horizontal', 'role'=>'form', 'route'=>'admin.settings', 'method'=>'put']) }}
                <section class="row">
                    <div class="col-md-5">
                        <!-- site name -->
                        <div class="form-group {{ $errors->has('site_name') ? 'has-error' : '' }}">
                            {!! Form::label('site_name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('site_name', $site_name, ['class' => 'form-control','placeholder' => 'Site Name', 'required']) !!}
                                {!! $errors->first('site_name', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site url -->
                        <div class="form-group {{ $errors->has('site_url') ? 'has-error' : '' }}">
                            {!! Form::label('site_url', 'Url', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_url', $site_url, ['class' => 'form-control','placeholder' => 'Site Url', 'required']) !!}
                                {!! $errors->first('site_url', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site Address -->
                        <div class="form-group {{ $errors->has('site_address') ? 'has-error' : '' }}">
                            {!! Form::label('site_address', 'Address', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('site_address', $site_address, ['class' => 'form-control','placeholder' => 'Site Address', 'required']) !!}
                                {!! $errors->first('site_address', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site Phone -->
                        <div class="form-group {{ $errors->has('site_phone') ? 'has-error' : '' }}">
                            {!! Form::label('site_phone', 'Phone', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('site_phone', $site_phone, ['class' => 'form-control','placeholder' => 'Site Phone', 'required']) !!}
                                {!! $errors->first('site_phone', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site Phone -->
                        <div class="form-group {{ $errors->has('site_email') ? 'has-error' : '' }}">
                            {!! Form::label('site_email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::email('site_email', $site_email, ['class' => 'form-control','placeholder' => 'Site Email', 'required']) !!}
                                {!! $errors->first('site_email', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site Fax -->
                        <div class="form-group {{ $errors->has('site_fax') ? 'has-error' : '' }}">
                            {!! Form::label('site_fax', 'Fax', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('site_fax', $site_fax, ['class' => 'form-control','placeholder' => 'Site Fax', 'required']) !!}
                                {!! $errors->first('site_fax', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div> <!-- ./col -->


                    <div class="col-md-7">
                        <!-- site slogan -->
                        <div class="form-group {{ $errors->has('site_slogan') ? 'has-error' : '' }}">
                            {!! Form::label('site_slogan', 'Slogan', ['class' => 'col-sm-3 control-label', 'method'=>'post']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('site_slogan', $site_slogan, ['class' => 'form-control', 'rows' => '2', 'placeholder' => 'Site Slogan', 'required']) !!}
                                {!! $errors->first('site_slogan', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site description -->
                        <div class="form-group {{ $errors->has('site_desc') ? 'has-error' : '' }}">
                            {!! Form::label('site_desc', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('site_desc', $site_desc, ['class' => 'form-control', 'rows' => '4', 'placeholder' => 'Site Description', 'required']) !!}
                                {!! $errors->first('site_desc', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!-- site bio -->
                        <div class="form-group {{ $errors->has('site_bio') ? 'has-error' : '' }}">
                            {!! Form::label('site_bio', 'Bio', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('site_bio', $site_bio, ['class' => 'form-control','rows' => '5', 'placeholder' => 'Site Bio', 'required']) !!}
                                {!! $errors->first('site_bio', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div> <!-- ./col -->
                </section>
                <section class="row">
                    <div class="alert alert-info">Social Networks</div>
                    <div class="col-md-5">
                        <!-- site facebook -->
                        <div class="form-group {{ $errors->has('site_facebook') ? 'has-error' : '' }}">
                            {!! Form::label('site_facebook', 'Facebook', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_facebook', $site_facebook, ['class' => 'form-control','placeholder' => 'Site Facebook', 'required']) !!}
                                {!! $errors->first('site_facebook', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- site googleplus -->
                        <div class="form-group {{ $errors->has('site_googleplus') ? 'has-error' : '' }}">
                            {!! Form::label('site_googleplus', 'Googleplus', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_googleplus', $site_googleplus, ['class' => 'form-control','placeholder' => 'Site Googleplus']) !!}
                                {!! $errors->first('site_googleplus', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- site Youtube -->
                        <div class="form-group {{ $errors->has('site_youtube') ? 'has-error' : '' }}">
                            {!! Form::label('site_youtube', 'Youtube', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_youtube', $site_youtube, ['class' => 'form-control','placeholder' => 'Site Youtube']) !!}
                                {!! $errors->first('site_youtube', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <!-- site Twitter -->
                        <div class="form-group {{ $errors->has('site_twitter') ? 'has-error' : '' }}">
                            {!! Form::label('site_twitter', 'Twitter', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_twitter', $site_twitter, ['class' => 'form-control','placeholder' => 'Site Twitter', 'required']) !!}
                                {!! $errors->first('site_twitter', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- site Instagram -->
                        <div class="form-group {{ $errors->has('site_instagram') ? 'has-error' : '' }}">
                            {!! Form::label('site_instagram', 'Instagram', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::url('site_instagram', $site_instagram, ['class' => 'form-control','placeholder' => 'Site Instagram', 'required']) !!}
                                {!! $errors->first('site_instagram', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="col-md-7">
                        {{ Form::submit('Update Details', ['class'=>'btn btn-lg bg-green pull-right']) }}
                    </div>
                </section>
            {{ Form::close() }}
        </div>
    </div>

    <div class="panel panel-flatborder-left-lg border-left-success">
          <div class="panel-heading">
             <h6 class="panel-title text-bold">Update Site Logo</h6>
             <div class="heading-elements">
                 <span class="heading-text"><i class="icon-clock"></i> {{ date('j F, Y') }} </span>
             </div>
          </div>
          <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted">This is where the site logo resides</p>
                    {{ Html::image('/img/logo-area.jpg', 'Logo Area', ['class'=>'responsive-image img-responsive']) }}
                    <hr/>
                </div>
                <div class="col-md-6" id="main-logo-section-container">
                    <h4 class="text-muted">Site's Current Main Logo</h4>
                    <div id="main-logo-section-holder" style="display: none;"></div>
                    {{ Html::image('/img/' . $site_logo, $site_logo, ['class'=>'responsive-image img-responsive', 'id'=>'main-logo']) }}
                <div id="main-logo2"></div>
                </div>
                <div class="col-md-6">
                    <h4 class="text-muted"><b>Main Logo: </b>Drag and Drop to auto-update the site's main logo</h4>
                    <form action="{{ route('admin.update-logo') }}" class="dropzone" id="site-main-logo" enctype="multipart/form-data">
                        <input type="file" name="file">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::script('/backend/tinymce/tinymce.min.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({
                selector : "textarea.tinymce",
                plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
                toolbar : "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link "
            }); 
        }); // End of Document Ready
    </script>

    {{ Html::script('/backend/js/dropzone.min.js') }}

    <script type="text/javascript">
        // Dropzone class
        Dropzone.autoDiscovery = false; 
        // autodiscover automatically finds and selects a form, hence we will turn it off
        
        var _token = $('meta[name="_token"]').attr('content');
        console.log(_token);

        var myDropzone = new Dropzone("#site-main-logo", {
            url: "{{ route('admin.update-logo') }}",
            method: "POST",
            maxFilesize: 1,
            acceptedFiles: 'image/*, .jpg, .png'
            // headers: {
            //     'X-CSRFToken': token
            // }
        });

        // update DOM with new logo after success
        myDropzone.on("success", function(file, response) {
            myDropzone.removeFile(file); 
            $("#main-logo-section-holder").load("{{ route('admin.main-logo') }}");
            var embe = $.get(
                "{{ route('admin.main-logo') }}"
            ).success(function(data) {
                // console.log(data);
                var new_logo = data;
                var path = '/img/new_logo';
                path = path.replace('new_logo', new_logo);
                // console.log(path);
                $("#main-logo").attr('src', path);
            });
        });

    </script>  


@stop
