@extends('admin.layouts.main')
@php 
	$downloads_this_week = [34,56,90,24,67,11,50];
	$most_downloaded_apps = [
		['appname'=> 'Audi A9', 'downloads'=>80], 
		['appname'=> 'Mercedes Benz', 'downloads'=>67], 
		['appname'=> 'Nissan Salon', 'downloads'=>55]
	];
	// dd($most_downloaded_apps);
	$date_today = date('Y-m-d');
	$previous_week = date('Y-m-d', strtotime('-1 week'));
	$days = [];
	for($i=7; $i > 0; $i--) {
		array_push($days, date('D', strtotime('-'.$i.' days')));
	}
	
@endphp

@section('content')
	<div class="row">
		<div class="col-md-4 col-xs-6">
	        <a href="#" type="button" class="btn bg-info-400 btn-block btn-float btn-float-lg text-size-small legitRipple">
					<span style="float: left; font-size: 40px;">{{ number_format($apps_count) }}</span>
		          		<i class="fa fa-clipboard" style="float: right; font-size: 50px; padding-right: 20px;"></i>
	       	 	<div class="clearfix"></div>
          		<p>Products(Apps)</p>
	      	</a>
	    </div>
	    <div class="col-md-4 col-xs-6">
	      	<a href="#" type="button" class="btn bg-warning-400 btn-block btn-float btn-float-lg text-size-small legitRipple">
					<span style="float: left; font-size: 40px;">{{ number_format($downloads_count) }}</span>
		          		<i class="fa fa-calendar" style="float: right; font-size: 50px; padding-right: 20px;"></i>
	       	 	<div class="clearfix"></div>
          		<p>Downloads</p>
	      	</a>
	    </div>
	    <div class="col-md-4 col-xs-6">
	    	<a href="#" type="button" class="btn bg-success-400 btn-block btn-float btn-float-lg text-size-small legitRipple">
					<span style="float: left; font-size: 40px;">{{ number_format(390000) }}</span>
		          		<i class="fa fa-bar-chart" style="float: right; font-size: 50px; padding-right: 10px;"></i>
	       	 	<div class="clearfix"></div>
          		<p>Yearly Earnings</p>
	      	</a>
	    </div>
   	</div>
   	<p></p>
	<div class="well">
		<h4 class="text-muted">Hello there, {{ Auth::user()->fname }}. Welcome to this part of the world</h4>
	</div>
	<p></p>
   	<p></p>
	
	{{-- chart js --}}
   	<div class="panel panel-flatborder-left-lg border-left-success timeline-content">
        <div class="panel-body">
			<style>
				canvas {
					-moz-user-select: none;
					-webkit-user-select: none;
					-ms-user-select: none;
				}
				.chart-container {
					display: inline-block;
				}
				.chart-left {
					width: 500px;
				}
				.chart-right {
					width: 400px;
				}
			</style>
			<div class="row">
				<div class="col-md-12">
					<canvas id="chart-legend-downloads"></canvas>
				</div>
				<div class="col-md-6">
					<canvas id="chart-legend-popular" class=""></canvas>
				</div>
				<div class="col-md-6">
					<canvas id="chart-legend-popular-bar" class=""></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-flatborder-left-lg border-left-info">
        <div class="panel-heading">
            <h6 class="panel-title text-bold">Common Elements for Common Operations (Copy Paste)</h6>
            <div class="heading-elements">
                <span class="heading-text"><i class="icon-clock fg-purple"></i> {{ date('j F, Y') }} </span>
            </div>
        </div>
        <div class="panel-body">
			<div class="row">
				<div class="col-sm-4">
					<p class="text-muted">Form Horizontal with Date Time Picker</p>
					{{ Form::open(['class'=>'form-horizontal', 'role'=>'form']) }}
						{{-- date and Time picker --}}
						<div class="form-group {{ $errors->has('date_time_in') ? 'has-error' : '' }}">
							{{ Form::label('date_time_in', 'Date Time Picker', ['class' => 'col-sm-5 control-label']) }}
							<div class="col-sm-7">
								{{ Form::text('date_time_in', date('Y-m-d H:i a', strtotime('+1 days')), ['class' => 'form-control datetimepicker', 'id'=>'date_time_in', 'placeholder' => 'Time In', 'required']) }}
								{!! $errors->first('date_time_in', '<span class="help-block">:message</span>') !!}
							</div>
						</div>
						{{-- Time picker only --}}
						<div class="form-group {{ $errors->has('time_in') ? 'has-error' : '' }}">
							{{ Form::label('time_in', 'Time Picker', ['class' => 'col-sm-5 control-label']) }}
							<div class="col-sm-7">
								{{ Form::text('time_in', date('H:i a', strtotime('+1 hours')), ['class' => 'form-control onlytimepicker', 'id'=>'time_in', 'placeholder' => 'Time In', 'required']) }}
								{!! $errors->first('time_in', '<span class="help-block">:message</span>') !!}
							</div>
						</div>
						{{-- Date picker only --}}
						<div class="form-group {{ $errors->has('date_in') ? 'has-error' : '' }}">
							{{ Form::label('date_in', 'Date Picker', ['class' => 'col-sm-5 control-label']) }}
							<div class="col-sm-7">
								{{ Form::text('date_in', date('Y-m-d', strtotime('+1 days')), ['class' => 'form-control onlydatepicker', 'id'=>'time_in', 'placeholder' => 'Time In', 'required']) }}
								{!! $errors->first('date_in', '<span class="help-block">:message</span>') !!}
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
        </div>
	</div>

@stop

@section('scripts')
	{{ Html::script('/backend/tinymce/tinymce.min.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({
                selector : "textarea.tinymce",
                plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
                toolbar : "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link "
            }); 
        }); // End of Document Ready
    </script>

	{{-- chart  --}}
	{{ Html::script('/backend/js/chart.min.js') }}
	<script type="text/javascript">
		var y=[];
		var z=[];
		var popular=[];
		var popular_count=[];
		var ctx = document.getElementById("chart-legend-downloads").getContext("2d");
		var ctx2 = document.getElementById("chart-legend-popular").getContext("2d");
		var ctx3 = document.getElementById("chart-legend-popular-bar").getContext("2d");

		// inject values into variable arrays
		// hold no. of downloads during the week
		@foreach($downloads_this_week as $downloads)
			y.push(parseInt( {{ $downloads }} ));
		@endforeach
		//console.log(y);

		// hold days names
		@foreach($days as $the_day)
			z.push("{{ $the_day }}")
		@endforeach

		// hold polular apps names
		@foreach($most_downloaded_apps as $popular)
			popular.push("{{ $popular['appname'] }}")
		@endforeach

		// hold polular apps count
		@foreach($most_downloaded_apps as $popular)
			popular_count.push("{{ $popular['downloads'] }}")
		@endforeach

		var config = {
			type: 'line',
			data: {
				datasets: [{
					label: "Downloads This Week",
					data: y,
					backgroundColor: "#9684FB",
				}],
				labels: z,
				borderWidth: 2
			},
				options: {
				responsive: true,
				//legend: {
				//	position: legendPosition,
				//},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Day'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Downloads'
						}
					}]
				},
				title: {
					display: true,
					text: 'Quick Peek on Ongoing Weekly Download Trend'
				}
			}
		};

		var configPopular = {
			type: 'pie',
			data: {
				datasets: [{
					label: "Popular Downloads",
					data: popular_count,
					backgroundColor: [
						"#F7464A",
						"#46BFBD",
						"#FDB45C",
						"#f56954",
						"#00a65a",
						"#f39c12",
						"#00c0ef",
						"#FF80FF",
						"#8080FF",
						"#CC00CC",
						"#FFFF00",
						"#FF8040",
						"#8000FF",
						"#FF8040",
						"#0000FF",
						"#808000",
						"#F00000"
					],
				}],
				labels: popular
			},
				options: {
				responsive: true
			}
		};
		
		var configPopularBar = {
			type: 'bar',
			data: {
				datasets: [{
					label: "Popular Apps",
					data: popular_count,
					backgroundColor: [
						"#F7464A",
						"#46BFBD",
						"#FDB45C",
						"#f56954",
						"#00a65a",
						"#f39c12",
						"#00c0ef",
						"#FF80FF",
						"#8080FF",
						"#CC00CC",
						"#FFFF00",
						"#FF8040",
						"#8000FF",
						"#FF8040",
						"#0000FF",
						"#808000",
						"#F00000"
					],
				}],
				labels: popular
			},
				options: {
				responsive: true
			}
		};
		// console.log(config);
		// draw pie chart
		$(document).ready(function() {
			var my_pie = new Chart(ctx, config);                                
			var my_pie2 = new Chart(ctx2, configPopular);                                
			var my_pie3 = new Chart(ctx3, configPopularBar);                                
		});
	</script>
@endsection
