<section class="sidebar">
  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      {{ Html::image('/backend/img/' . Auth::user()->avatar, 'Avatar', ['class'=>'img-circle']) }}
    </div>
    <div class="pull-left info">
      <p>{{ ucfirst(Auth::User()->name) }}</p>
      <!-- Status -->
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form (Optional) -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
      <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search fg-purple"></i></button>
      </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- Sidebar Menu -->
  <ul class="sidebar-menu">
    <li class="header">NAVIGATION</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="{{ check_active('dashboard', 'active') }}" ><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fg-green"></i> <span>Dashboard</span></a></li>

    <!-- item 1 -->
    <li class="treeview  {{ check_active('Apps', 'active') }} ">
      <a href=""><i class="fa fa-folder-open fg-blue"></i> <span>Apps</span> <i class="fa fa-angle-left pull-right fg-brown"></i></a>
      <ul class="treeview-menu">
        <li><a href="{{ route('admin.dashboard') }}">All Apps</a></li>
      </ul>
    </li>


    <!-- item 5 -->
    <li class="treeview  {{ check_active('Settings', 'settings') }} ">
      <a href=""><i class="fa fa-cogs fg-red"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="{{ route('admin.settings') }}">All Settings</a></li>
      </ul>
    </li>

    <!-- item 6 -->
    <li>
      <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span>Front End</span> </a>
    </li>

    <li>
      <a href="#" class="has-ul legitRipple"><i class="ion-stats-bars fg-blue"></i> <span>Stats</span></a>
      <ul class="hidden-ul">
          <li><a href="{{ url('stats') }}" class="legitRipple"><i class="ion-arrow-graph-up-right"></i>All Stats</a></li>
          <li><a href="{{ url('stats?page=summary') }}" class="legitRipple"><i class="ion-arrow-graph-down-left"></i>Summary</a></li>
          <li><a href="{{ url('stats??days=0') }}" class="legitRipple"><i class="ion-ios-location"></i>Today</a></li>
          <li><a href="{{ url('stats??days=7') }}" class="legitRipple"><i class="ion-arrow-graph-up-right"></i>Weekly</a></li>
          <li><a href="{{ url('stats??days=30') }}" class="legitRipple"><i class="ion-android-compass"></i>Monthly</a></li>
          <li><a href="{{ url('stats??days=365') }}" class="legitRipple"><i class="ion-android-calendar"></i>Yearly</a></li>
          <li><a href="{{ url('stats?page=visits') }}" class="legitRipple"><i class="ion-arrow-graph-down-right"></i>Visits</a></li>
          <li><a href="{{ url('stats?page=users') }}" class="legitRipple"><i class="ion-person"></i>Users</a></li>
          <li><a href="{{ url('stats?page=events') }}" class="legitRipple"><i class="ion-ios-bolt"></i>Events</a></li>
          <li><a href="{{ url('stats?page=errors') }}" class="legitRipple"><i class="ion-alert"></i>Errors</a></li>
      </ul>
  </li>

  </ul>
  <!-- /.sidebar-menu -->
</section>