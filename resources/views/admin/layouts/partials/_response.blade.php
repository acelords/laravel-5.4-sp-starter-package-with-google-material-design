@if(Session::has('success'))
    <div id="successModalTrigger" data-content="{!! Session::get('success') !!}"></div>
    
@endif

@if(Session::has('info'))
    <div id="infoModalTrigger" data-content="{!! Session::get('info') !!}"></div>
    
@endif

@if(Session::has('warning'))
    <div id="warningModalTrigger" data-content="{!! Session::get('warning') !!}"></div>
    
@endif

@if(Session::has('fail'))
    <div id="failModalTrigger" data-content="{!! Session::get('fail') !!}"></div>
@endif
