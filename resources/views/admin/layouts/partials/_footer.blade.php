<div class="navbar navbar-default navbar-fixed-bottom footer">
    <ul class="nav navbar-nav visible-xs-block">
        <li><a class="text-center legitRipple collapsed" data-toggle="collapse" data-target="#footer" aria-expanded="false"><i class="fa fa-spin ion-ios-reload"></i></a>
        </li>
    </ul>

    <div class="navbar-collapse collapse" id="footer" aria-expanded="false" style="height: 1px;">
        <div class="navbar-text">
            &copy; {{ date('Y') }}. <a href="{{ $site_url }}" class="navbar-link">{{ $site_name }}</a> by  
            <a href="http://google.com/+LexxYungCarter" class="navbar-link" target="_blank">Lexx,</a>
            <a href="http://acelords.space" class="navbar-link" target="_blank">AceLords</a> and 
            <a href="javascript:;" class="navbar-link" target="_blank">friends.</a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('admin.dashboard') }}" class="legitRipple">About</a></li>
                <li><a href="{{ route('admin.dashboard') }}" class="legitRipple">Contact</a></li>
            </ul>
        </div>
    </div>
</div>