<!-- Second navbar -->
<div class="navbar navbar-inverse navbar-transparent" id="navbar-second">
    <ul class="nav navbar-nav visible-xs-block">
        <li><a class="text-center collapsed legitRipple" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="fa fa-align-justify"></i></a>
        </li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav navbar-nav-material">
            <li class="{{ check_active('', 'active') }}"><a href="{{ route('admin.dashboard') }}" class="legitRipple">Dashboard</a>
            </li>

            {{-- apps --}}
            <li class="dropdown mega-menu {{ check_active('Datatables', 'active') }}">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" data-hover="dropdown"><span class="fa fa-android"></span> Apps (Datatables) <span class="fa fa-windows"></span></a>

                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-body">
                        <span class="menu-heading underlined">Available Apps</span>
                        <ul class="menu-list dd-menu">
                            <li class="dd-parent">
                                <a href="#" class="dd-parent-a"><i class="fa fa-check fg-purple"></i> All Apps</a>
                            </li>
                            <li class="dd-parent">
                                <a href="{{ route('admin.datatables') }}" class="dd-parent-a"><i class="fa fa-table fg-brown"></i> Search App (Datatables)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            
        </ul>
    </div>
</div>
<!-- /second navbar -->