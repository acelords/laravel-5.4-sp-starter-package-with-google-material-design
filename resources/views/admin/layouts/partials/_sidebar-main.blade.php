
<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="sidebar-user-material">
                <div class="category-content" style="background: url('{{ route('user-avatar', 'avatar-back.jpg') }}'); background-size: cover; background-position: center;">
                    <div class="sidebar-user-material-content">
                        <a href="#" class="legitRipple">
                            {{ Html::image( route('user-avatar', Auth::user()->avatar), Auth::user()->avatar, ['class'=>'img-circle img-responsive']) }}
                        </a>
                        <h6>{{ ucfirst(Auth::user()->name) }}</h6>
                        <span class="text-size-small">{{ ucfirst(Auth::user()->email) }}</span>
                    </div>

                    <div class="sidebar-user-material-menu">
                        <a href="#user-nav" data-toggle="collapse" class="legitRipple"><span>My account</span> <i class="fa fa-angle-down fg-green"></i></a>
                    </div>
                </div>

                <div class="navigation-wrapper collapse" id="user-nav">
                    <ul class="navigation">
                        <li><a href="{{ route('admin.dashboard') }}" class="legitRipple"><i class="fa fa-user fg-purple"></i> <span>My profile</span></a>
                        </li>
                        <li><a href="#" class="legitRipple"><i class="fa fa-comments fg-green"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#" class="legitRipple"><i class="fa fa-spin fa-cog fg-red"></i> <span>Account settings</span></a>
                        </li>
                        <li><a href="{{url('/logout') }}" class="legitRipple"><i class="fa fa-power-off fg-purple"></i> <span>Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="" data-original-title="Main pages"></i></li>
                    <li><a href="{{ route('admin.dashboard') }}" class="legitRipple"><i class="fa fa-home fg-blue"></i> <span>Dashboard</span></a></li>

                    {{-- yield('sidebar') --}}

                    {{-- apps section --}}
                    <li class="navigation-header"><span>Apps Section</span> <i class="fa fa-android fg-green" title="Apps Section" data-original-title="Apps pages"></i></li>
                    <li>
                        <a href="#" class="has-ul legitRipple"><i class="fa fa-list-ul fg-orange"></i> <span>Apps</span></a>
                        <ul class="hidden-ul">
                            <li><a href="{{ route('admin.dashboard') }}" class="legitRipple"><i class="fa fa-android fg-green"></i> Android Apps</a></li>
                            <li><a href="{{ route('admin.dashboard') }}" class="legitRipple"><i class="fa fa-apple fg-blue"></i> IOS Apps</a></li>
                            <li><a href="{{ route('admin.dashboard') }}" class="legitRipple"><i class="fa fa-windows fg-orange"></i> Windows Apps</a></li>
                        </ul>
                    </li>
                    
                    {{-- Reports Section --}}
                    <li class="navigation-header"><span>Reports Section</span> <i class="fa fa-clipboard fg-brown" title="Reports Section" data-original-title="Reports pages"></i></li>
                    
                    <li class="navigation-header"><span>Site Settings Section</span> <i class="fa fa-cogs fg-red" title="" data-original-title="Site Settings"></i></li>
                    <li>
                        <a href="#" class="has-ul legitRipple"><i class="fa fa-cogs fg-red"></i> <span>Site Details</span></a>
                        <ul class="hidden-ul">
                            <li><a href="{{ route('admin.settings') }}" class="legitRipple"><i class="fa fa-cog fa-spin fg-orange"></i>Settings</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="has-ul legitRipple"><i class="fa fa-line-chart fg-blue"></i> <span>Stats</span></a>
                        <ul class="hidden-ul">
                            <li><a href="{{ url('stats') }}" class="legitRipple"><i class="fa fa-area-chart fg-green"></i>All Stats</a></li>
                            <li><a href="{{ url('stats?page=summary') }}" class="legitRipple"><i class="fa fa-clipboard fg-orange"></i>Summary</a></li>
                            <li><a href="{{ url('stats??days=0') }}" class="legitRipple"><i class="fa fa-angle-down fg-purple"></i>Today</a></li>
                            <li><a href="{{ url('stats??days=7') }}" class="legitRipple"><i class="fa fa-align-center fg-red"></i>Weekly</a></li>
                            <li><a href="{{ url('stats??days=30') }}" class="legitRipple"><i class="fa fa-compass fg-brown"></i>Monthly</a></li>
                            <li><a href="{{ url('stats??days=365') }}" class="legitRipple"><i class="fa fa-calendar fg-blue"></i>Yearly</a></li>
                            <li><a href="{{ url('stats?page=visits') }}" class="legitRipple"><i class="fa fa-exchange fg-green"></i>Visits</a></li>
                            <li><a href="{{ url('stats?page=users') }}" class="legitRipple"><i class="fa fa-group fg-purple"></i>Users</a></li>
                            <li><a href="{{ url('stats?page=events') }}" class="legitRipple"><i class="fa fa-flash fg-brown"></i>Events</a></li>
                            <li><a href="{{ url('stats?page=errors') }}" class="legitRipple"><i class="fa fa-exclamation-circle fg-red"></i>Errors</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /main sidebar -->