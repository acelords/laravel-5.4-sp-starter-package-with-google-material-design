<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-transparent">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ Html::image('/img/'. $site_logo , $site_logo, ['class'=>'img-responsive']) }}
        </a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple"><i class="fa fa-navicon"></i></a>
            </li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <p class="navbar-text">{{ $site_slogan }}</p>

        <!-- <ul class="nav navbar-nav">
            <li><a href="#" class="legitRipple">Upgrade</a>
            </li>
        </ul> -->

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-refresh"></i>
                        <span class="visible-xs-inline-block position-right">Activity</span>
                        <span class="status-mark border-pink-300"></span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Activity
                            <ul class="icons-list">
                                <li><a href="#"><i class="fa fa-list"></i></a>
                                </li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body width-350">
                            <li class="media">
                                <div class="media-left">
                                    <a href="#" class="btn bg-success-400 btn-rounded btn-icon btn-xs legitRipple"><i class="fa-at"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Test User</a> mentioned you in a post "Angular JS. Tips and tricks"
                                    <div class="media-annotation">4 minutes ago</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" data-hover="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="visible-xs-inline-block position-right">Messages</span>
                    </a>

                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            Messages
                            <ul class="icons-list">
                                <li><a href="#"><i class="fa fa-list-alt"></i></a>
                                </li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-left">
                                    {{ Html::image( route('user-avatar', Auth::user()->avatar), Auth::user()->avatar, ['class'=>'img-circle img-sm']) }}
                                    <span class="badge bg-danger-400 media-badge">5</span>
                                </div>

                                <div class="media-body">
                                    <a href="http://google.com/+LexxYungCarter" class="media-heading">
                                        <span class="text-semibold">Lexx YungCarter</span>
                                        <span class="media-annotation pull-right">{{ date('H:i', strtotime('-10 minutes')) }}</span>
                                    </a>

                                    <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                </div>
                            </li>
                        </ul>
                        
                        <ul class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-left">
                                    {{ Html::image( route('user-avatar', Auth::user()->avatar), Auth::user()->avatar, ['class'=>'img-circle img-sm']) }}
                                    <span class="badge bg-danger-400 media-badge">5</span>
                                </div>

                                <div class="media-body">
                                    <a href="http://google.com/+LexxYungCarter" class="media-heading">
                                        <span class="text-semibold">Benson Githinji</span>
                                        <span class="media-annotation pull-right">{{ date('H:i', strtotime('-15 minutes')) }}</span>
                                    </a>

                                    <span class="text-muted">am in love with this expressive framework...</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle legitRipple" data-toggle="dropdown" data-hover="dropdown">
                        {{ Html::image( route('user-avatar', Auth::user()->avatar), Auth::user()->avatar, ['class'=>'']) }}
                        <span>{{ ucfirst(Auth::user()->name) }}</span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-user fg-blue"></i> My profile</a>
                        </li>
                        <li><a href="#"><span class="badge bg-blue pull-right">20</span> <i class="fa fa-comments fg-green"></i> Messages</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ route('admin.settings') }}"><i class="fa fa-spin fa-cog fg-red"></i> Account settings</a>
                        </li>
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off fg-purple"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /main navbar -->