@if(Session::has('success'))
    <div id="successModalTrigger"></div>
    <div id="successModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Success" aria-hidden="true">
      <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header alert-success">
                <button type="button" class="close text-white" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mySuccessModal" style="position: relative;">
                	<span style="font-size: 50px; color: white;"><i class="fa fa-check-circle-o"></i></span> 
                	<span style="position: absolute; top: 20px; right: 30px;">{{ Session::get('success') }}</span>
                </h4>
              </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
@endif
@if(Session::has('fail'))
	<!-- Small modal -->
    <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#failModal">Failed</button> -->
    <div id="failModalTrigger"></div>
    <div id="failModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Error!!" aria-hidden="true">
      <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header alert-danger">
                <button type="button" class="close text-white" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mySuccessModal" style="position: relative;">
                	<span style="font-size: 50px; color: red;"><i class="fa fa-times-circle-o"></i></span> 
                	<span style="position: absolute; top: 20px; right: 30px;">{{ Session::get('fail') }}</span>
                </h4>
            </div>
          </div><!-- /.modal-content -->
      </div>
    </div>
@endif
