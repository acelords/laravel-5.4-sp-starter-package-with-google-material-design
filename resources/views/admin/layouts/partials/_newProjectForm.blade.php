<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
            {{ Form::label('title', 'Title', ['class' => 'control-label']) }}
            {{ Form::text('title', null, ['class' => 'form-control','placeholder' => 'Testimonial Title eg Honored!', 'required']) }}
            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
            {{ Form::label('category', 'Category', ['class' => 'control-label']) }}
            <select name="category" id="category" class="form-control" required>
                <option value="blogs">Blog Website</option>
                <option value="commercial">Commercial Project</option>
                <option value="graphics">Graphic Design</option>
                <option value="themes">Theme</option>
                <option value="quick">Quick Project</option>
            </select>
            {!! $errors->first('category', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
            {{ Form::label('url', 'Url', ['class' => 'control-label']) }}
            {{ Form::text('url', null, ['class' => 'form-control','placeholder' => 'http://example.com', 'required']) }}
            {!! $errors->first('url', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('project_url') ? 'has-error' : '' }}">
            {{ Form::label('project_url', 'Project URL', ['class' => 'control-label']) }}
            {{ Form::text('project_url', null, ['class' => 'form-control','placeholder' => 'http://project-url.com']) }}
            {!! $errors->first('project_url', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('date_of_release') ? 'has-error' : '' }}">
            {{ Form::label('date_of_release', 'Date of release', ['class' => 'control-label']) }}
            {{ Form::text('date_of_release', null, ['class' => 'form-control datetimepicker', 'required']) }}
            {!! $errors->first('date_of_release', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('developers') ? 'has-error' : '' }}">
            {{ Form::label('developers', 'Developers', ['class' => 'control-label']) }}
            {{ Form::text('developers', 'AceLords', ['class' => 'form-control','placeholder' => 'Ace Lords', 'required']) }}
            {!! $errors->first('developers', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>



<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    {{ Form::label('description', 'Description', ['class' => 'control-label']) }}
    {{ Form::textarea('description', null, ['class' => 'form-control','placeholder' => 'I Love ' . $site_name . ' LTD', 'required']) }}
    {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('quote1') ? 'has-error' : '' }}">
            {{ Form::label('quote1', 'Quote 1', ['class' => 'control-label']) }}
            {{ Form::text('quote1', null, ['class' => 'form-control','placeholder' => 'quote1', 'required']) }}
            {!! $errors->first('quote1', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('quote2') ? 'has-error' : '' }}">
            {{ Form::label('quote2', 'Quote 2', ['class' => 'control-label']) }}
            {{ Form::text('quote2', null, ['class' => 'form-control','placeholder' => 'quote2']) }}
            {!! $errors->first('quote2', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('quote3') ? 'has-error' : '' }}">
            {{ Form::label('quote3', 'Quote 3', ['class' => 'control-label']) }}
            {{ Form::text('quote3', null, ['class' => 'form-control','placeholder' => 'quote3']) }}
            {!! $errors->first('quote3', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer1_name') ? 'has-error' : '' }}">
            {{ Form::label('designer1_name', 'Designer1 Name', ['class' => 'control-label']) }}
            {{ Form::text('designer1_name', 'Lexx YungCarter', ['class' => 'form-control','placeholder' => 'Designer1 Name', 'required']) }}
            {!! $errors->first('designer1_name', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer1_url') ? 'has-error' : '' }}">
            {{ Form::label('designer1_url', 'Designer1 URL', ['class' => 'control-label']) }}
            {{ Form::url('designer1_url', 'http://www.google.com/+LexxYungCarter', ['class' => 'form-control','placeholder' => 'Designer1 URL', 'required']) }}
            {!! $errors->first('designer1_url', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer1_email') ? 'has-error' : '' }}">
            {{ Form::label('designer1_email', 'Designer1 Email', ['class' => 'control-label']) }}
            {{ Form::email('designer1_email', 'lexx@acelords.space', ['class' => 'form-control','placeholder' => 'Designer1 Email', 'required']) }}
            {!! $errors->first('designer1_email', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer2_name') ? 'has-error' : '' }}">
            {{ Form::label('designer2_name', 'Designer2 Name', ['class' => 'control-label']) }}
            {{ Form::text('designer2_name', null, ['class' => 'form-control','placeholder' => 'Designer2 Name']) }}
            {!! $errors->first('designer2_name', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer2_url') ? 'has-error' : '' }}">
            {{ Form::label('designer2_url', 'Designer2 URL', ['class' => 'control-label']) }}
            {{ Form::url('designer2_url', null, ['class' => 'form-control','placeholder' => 'Designer2 URL']) }}
            {!! $errors->first('designer2_url', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('designer2_email') ? 'has-error' : '' }}">
            {{ Form::label('designer2_email', 'Designer2 Email', ['class' => 'control-label']) }}
            {{ Form::email('designer2_email', null, ['class' => 'form-control','placeholder' => 'Designer2 Email']) }}
            {!! $errors->first('designer2_email', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('img1') ? 'has-error' : '' }}">
            {{ Form::label('img1', 'Main Image', ['class' => 'control-label']) }}
            <input type="file" name="img1" class="form-control" id="img1" required/>
            {!! $errors->first('img1', '<span class="help-block">:message</span>') !!}
        </div> 
    </div>
    
</div>
