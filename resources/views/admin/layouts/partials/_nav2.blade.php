<!-- Page header content -->
<div class="page-header-content">
    <div class="page-title">
        <h4><a href="{{ route('admin.dashboard') }}" class="text-white">Dashboard</a> <small>Hello, {{ ucfirst(Auth::user()->fname) }}</small></h4>
        <a class="heading-elements-toggle"><i class="fa fa-angle-double-right"></i></a><a class="heading-elements-toggle"><i class="fa fa-angle-double-right"></i></a>
    </div>

    <div class="heading-elements">
        <ul class="breadcrumb heading-text">
            <li><a href="{{ route('admin.dashboard')}}"><i class="fa fa-home position-left"></i> Home</a>
            </li>
            <li class="active">{{ $page }}</li>
        </ul>
    </div>
</div>
<!-- /page header content -->