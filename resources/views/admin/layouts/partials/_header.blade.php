<div class="page-header page-header-inverse bg-indigo">
  @include('admin.layouts.partials._nav1')
  @include('admin.layouts.partials._nav2')
  @include('admin.layouts.partials._nav3')

  <!-- Floating menu -->
  {{-- <ul class="fab-menu fab-menu-top-right" data-fab-toggle="click" data-fab-state="closed">
      <li>
          <a class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon legitRipple">
              <i class="fab-icon-open icon-plus3"></i>
              <i class="fab-icon-close icon-cross2"></i>
          </a>

          <ul class="fab-menu-inner">
              <li>
                  <div data-fab-label="Compose email">
                      <a href="#" class="btn btn-default btn-rounded btn-icon btn-float legitRipple">
                          <i class="icon-pencil"></i>
                      </a>
                  </div>
              </li>
              <li>
                  <div data-fab-label="Conversations">
                      <a href="#" class="btn btn-default btn-rounded btn-icon btn-float legitRipple">
                          <i class="icon-bubbles7"></i>
                      </a>
                      <span class="badge bg-primary-400">5</span>
                  </div>
              </li>
              <li>
                  <div data-fab-label="Chat with Jack">
                      <a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-float legitRipple">
                          <img src="./img/avatar_custom.png" class="img-responsive" alt=""/>
                      </a>
                      <span class="status-mark border-pink-400"></span>
                  </div>
              </li>
          </ul>
      </li>
  </ul> --}}
  <!-- /floating menu -->

</div>