<section class="content-header">
  <h1>
    {{ $page }}
    <small>{{ $page_desc }}</small>
  </h1>
  <ol class="breadcrumb" style="">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-{{ $icon }}"></i> Dashboard </a></li>
    <li class="active">{{ $page }}</li>
  </ol>
</section>