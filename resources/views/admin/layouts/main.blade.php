<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <title>{{ ucfirst($title) }} | Admin {{ ucfirst($site_name) }}</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Global stylesheets -->
    {{ Html::style('/backend/css/bootstrap.css') }}
    {{ Html::style('/backend/css/pnotify.custom.min.css') }}
    {{ Html::style('/backend/css/core.css') }}
    {{ Html::style('/backend/css/awesome-bootstrap-checkbox.css') }}
    {{ Html::style('/backend/css/components.css') }}
    {{ Html::style('/backend/css/colors.css') }}
    {{ Html::style('/backend/css/pace.css') }}
    {{ Html::style('/backend/css/jquery.datetimepicker.css') }}
    

    @yield('styles')
    <!-- /global stylesheets -->

    <!-- custom stylesheet -->
    {{ Html::style('/backend/css/custom.css') }}

    <!-- fonts -->
    {{ Html::style('/font-awesome-4.6.3/css/font-awesome.min.css') }}
    
  </head>
  
  <body class="pace-done navbar-bottom">
      <div class="pace  pace-inactive">
          <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
              <div class="pace-progress-inner"></div>
          </div>
          <div class="pace-activity"></div>
      </div>

    <!-- Page header. Includes all 3 navbars -->
    @include('admin.layouts.partials._header')

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

          <!-- Main sidebar -->
            @php  
                // route segment to avoid the placement of main sidebar
                if (
                    (Request::segment(2) == 'datatables' && Request::segment(3) == '') || 
                    (Request::segment(2) == 'apps' && Request::segment(3) == 'edit')
                ){ 
            @endphp
                {{-- do not include the sidebar, or include an optional slimer sidebar  --}}
                
            @php } else { @endphp
                {{-- include the sidebar --}}
                @include('admin.layouts.partials._sidebar-main')
            @php } @endphp

            <!-- Main content -->
            <div class="content-wrapper">
            @yield('content')
                
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

    <!-- Footer -->
    @include('admin.layouts.partials._footer')

    <!-- Response: success and fail -->
    @include('admin.layouts.partials._response') 

    <!-- Core JS files -->
    <!-- jQuery 2.1.4 -->
    {{ Html::script('/backend/js/pace.min.js') }}
    {{ Html::script('/backend/js/jquery.min.js') }}
    <!-- <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
    <!-- Bootstrap 3.3.5 -->
    {{ Html::script('/backend/js/bootstrap.min.js') }}
    {{ Html::script('/backend/js/blockui.min.js') }}
    {{ Html::script('/backend/js/nicescroll.min.js') }}
    {{ Html::script('/backend/js/drilldown.js') }}
    {{ Html::script('/backend/js/hover_dropdown.min.js') }}
    {{ Html::script('/backend/js/pnotify.custom.min.js') }}
    {{ Html::script('/js/jquery.datetimepicker.js') }}
    {{ Html::script('/backend/js/custom.js') }}
    <!-- /core JS files -->
   
    <!-- Theme JS files -->
    {{ Html::script('/backend/js/prism.min.js') }}
    {{ Html::script('/backend/js/app.js') }}
    {{ Html::script('/backend/js/ripple.min.js') }}
    <!-- /theme JS files -->
    
    @yield('scripts')
  </body>
</html>
