{{-- <a href="{{ route('admin.delete-apps-single', $product->id) }}" class="legitRipple"><i class="fa fa-trash fg-red"></i></a> --}}
@extends('admin.layouts.main')

@section('styles')
    {{ Html::style('/DataTables/datatables.min.css') }}
@stop

@section('content')
    <div class="panel panel-flatborder-left-lg border-left-success timeline-content">
        <div class="panel-heading">
            <h4 class="panel-title text-bold">Basic DataTables Implementation on Laravel</h4>
            <p>Get the basics of datatables implementation on Laravel 5+.</p>
            <div class="heading-elements">
                <span class="heading-text"><i class="icon-clock fg-purple"></i> {{ date('j F, Y') }} </span>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover" id="apps-table">
                    <thead class="bg-info-400">
                        <tr>
                            <td>#</td>
                            <td>Icon</td>
                            <td>Appname</td>
                            <td>Category</td>
                            <td>Filename</td>
                            <td>Extension</td>
                            <td>Created At</td>
                            <td>Last Updated</td>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{{-- modal --}}
<div class="modal fade" id="modal-apps" tabindex="-1" role="dialog" aria-labelledby="modal-apps" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="modal-title">My App</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-5">
                        <img src="/img/avatar.png" class="img-responsive" alt="My App" id="modal-app-icon">
                    </div>
                    <div class="col-sm-7">
                        <h3 class="text-muted"><em>Name:</em> <span id="modal-app-name">App Name</span></h3>
                        <hr/>
                        <h4><em>ID:</em> #<span id="modal-app-id">ID</span></h4>
                        <h5><em>Category:</em> <span id="modal-app-category">Category</span></h5>
                        <h6><em>Filename:</em> <span id="modal-app-filename">Filename</span></h6>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success legitRipple" id="modal-view-app">View App</button>
                <button type="button" class="btn btn-info legitRipple" id="modal-edit-app">Edit App</button>
                <button type="button" class="btn btn-danger legitRipple" id="modal-delete-app">Delete App</button>
                <button type="button" class="btn btn-link pull-left legitRipple" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop

@section('scripts')
    {{ Html::script('/DataTables/datatables.min.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            var the_app = null;

            var y={}; // hold categories

            // inject values into variable arrays
            @foreach($categories as $category)
                y[parseInt({{ $category->id }})] = "{{ $category->name }}";
            @endforeach
            // console.log(y);

            $('#apps-table').DataTable( {
                // "order": [ 1, "asc" ],
                dom: 'B<lf<t>ip>',
                buttons: [
                    'copy', 'excel', 'pdf'
                ],
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.fetch-products-ajax') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'appicon', name: 'icon' },
                    { data: 'appname', name: 'appname' },
                    { data: 'category_id', name: 'category_id' },
                    { data: 'filename', name: 'filename' },
                    { data: 'extension', name: 'extension' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' }
                ],
                columnDefs:[
                    {
                        render: function(data, type, row) {
                            return get_item_name(y, data); // category name
                        },
                        targets: 3
                    },
                    {
                        render: function(data, type, row) {
                            return lexx_date_formatter(data); // category name
                        },
                        targets: 6
                    },
                    {
                        render: function(data, type, row) {
                            return lexx_date_formatter(data); // category name
                        },
                        targets: 7
                    },
                    {
                        render: function(data, type, row) {
                            return '<img class="img-responsive" style="max-height: 40px;" src="/backend/img/apps/' + data + '">'; // category name
                        },
                        targets: 1
                    }
                ]
            });

            var table = $('#apps-table').DataTable();

            $('#apps-table tbody').on('click', 'tr', function () {
                var data = table.row( this ).data();
                the_app = data['id'];
                // get_stud_data(stud);
                // console.log(the_app);
                // alert( 'Student Adm_No '+data.fname+'\'s row' );
                $('#modal-apps').modal('show');
                $('#modal-apps #modal-app-id').text(the_app);
                $('#modal-apps #modal-app-name').text(data['appname']);
                $('#modal-apps #modal-app-category').text(get_item_name(y, data['category_id']));
                $('#modal-apps #modal-app-filename').text(data['filename']);
                $('#modal-apps #modal-app-icon').attr('src', '/backend/img/' + data['appicon']);
            });

            // View app button clicked
            $('#modal-view-app').on('click', function(e) {
                e.preventDefault();
                var app_id = $('#modal-apps #modal-app-id').text();
                if(app_id != '') {
                    var url = '{{ route("view-product", ":app_id") }}';
                    url = url.replace(':app_id', app_id);
                    window.location = url;                
                } else {
                    alert('Please Select an App first');
                }
            });
            
            // edit app button clicked
            $('#modal-edit-app').on('click', function(e) {
                e.preventDefault();
                var app_id = $('#modal-apps #modal-app-id').text();
                if(app_id != '') {
                    var url = '{{ route("admin.edit-product", ":app_id") }}';
                    url = url.replace(':app_id', app_id);
                    window.location = url;                
                } else {
                    alert('Please Select an App first');
                }
            });
            
            // delete app button clicked
            $('#modal-delete-app').on('click', function(e) {
                e.preventDefault();
                var app_id = $('#modal-apps #modal-app-id').text();
                if(app_id != '') {
                    var url = '{{ route("admin.delete-product", ":app_id") }}';
                    url = url.replace(':app_id', app_id);
                    window.location = url;                
                } else {
                    alert('Please Select an App first');
                }
            });

        });
    </script>
@stop
