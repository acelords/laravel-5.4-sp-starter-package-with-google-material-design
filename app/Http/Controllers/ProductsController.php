<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditSiteSettingsRequest; // edit site settings
use App\Http\Requests\AddAppRequest; // add app


use App\Product; // model
use App\SiteDetail; // model
use App\Category; // model
use Auth; // logged in user
use Image; // images
use File; // files
use Response; // responses
use Carbon\Carbon; // date formating

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->products = new Product();
        $this->categories = new Category();
        // $this->nav_items = array("android","blackberry","ios","html 5","java","symbian","windows phone","windows mobile");
        $this->nav_items = array("Android 4.4", "Andoid 5", "Android 5.1", "Android 6", "Android 7");
    }

    // fetch categories 
    protected function get_the_category_detail($cat_id, $field) {
        $all_cats = $this->categories->select(['id','folder'])->get();
        foreach($all_cats as $the_cat) {
            if($the_cat->id == $cat_id) {
                return $the_cat->$field;
            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // set up some things for demo purposes
        $previous_week = date('Y-m-d', strtotime('-1 week'));
        $downloads = collect([
            ['id'=>'1', 'created_at'=>date('Y-m-d', strtotime('-6 days'))],
            ['id'=>'2', 'created_at'=>date('Y-m-d', strtotime('-5 days'))],
            ['id'=>'3', 'created_at'=>date('Y-m-d', strtotime('-4 days'))],
            ['id'=>'4', 'created_at'=>date('Y-m-d', strtotime('-5 days'))],
            ['id'=>'5', 'created_at'=>date('Y-m-d', strtotime('-6 days'))],
            ['id'=>'6', 'created_at'=>date('Y-m-d', strtotime('-1 days'))],
        ]);
        // dd($downloads);

        return view('admin.index')
            ->withIcon('home')
            ->withTitle('dashboard')
            ->withPage('dashboard')
            ->withApps_count(Product::count())
            ->withCategories_count(Category::count())
            ->withDownloads_count(Product::sum('downloads'))
            ->withProducts($this->products->get())
            ->withNav_items($this->nav_items)
            ->withCategories($this->categories->get())
            // reporting
            ->withMost_downloaded_apps(Product::orderBy('downloads')->limit(5)->get());
            // ->withDownloads_this_week(Download::where('created_at', '>=', $previous_week)
                                // ->select(['id', 'created_at'])
                                // ->orderBy('created_at')
                                // ->get()
                                // ->groupBy(function($date){
                                //     return Carbon::parse($date->created_at)->format('d');
                                // })
            // )
            // ->withDownloads_this_week($downloads);

    }

    /**
     * Store a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AddAppRequest $request)
    {
        // dd($request);
        // initialize variables 
        $screenshots = [];

        // collect request
        $appname = $request->get('name');
        $category = $request->get('category');
        $device_type = $request->get('device_type');
        $description = $request->get('description');
        $nicename = str_replace(" ", "", $appname);
        
        $this->products->user_id = Auth::user()->id;
        $this->products->appname = $appname;
        $this->products->category_id = $category;
        $this->products->device_type = $device_type;
        $this->products->description = $description;
        $this->products->downloads = 0;

        //  saving the file apk
        if($request->hasFile('app')) {
            // get file 
            $app = $request->app->isValid();
            $extension = $request->file('app')->extension();
            $extension_to_save = $request->file('app')->getClientOriginalExtension();

            // check if extension is .exe or .dll 
            if($extension_to_save == 'exe' || $extension_to_save == 'dll') {
                return redirect()->back()->withInput()->withWarning('The system does not support that kind of file!');
            }

            $filename = $nicename . time() . '.' . $extension; // filename

            // $path = $request->app->storeAs('/public', $filename); // storepath, filename
            $path = public_path(). '/backend/apps/'; // destination path
            $request->app->move($path, $filename); // move

            // save into DB 
            $this->products->filename = $filename;
            $this->products->extension = $extension_to_save;
        }
        // dd($extension . ' ext : Filename ' . $filename . ' path ' . $path);

        // save icon 
        // get avatar
        if($request->hasFile('icon')) 
        {
            $image = $request->file('icon');

            $extension = $request->icon->getClientOriginalExtension();

            // check if extension is .exe or .dll 
            if($extension == 'exe' || $extension == 'dll') {
            	return redirect()->back()->withInput()->withWarning('That is not a valid image file! Please choose another one.');
            }

            $filename  = $nicename . '-icon-' . time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('/backend/img/' . $this->get_the_category_detail($category, 'folder') . '/' . 
                $filename); //location to save
            Image::make($image->getRealPath())->save($path);

            // save into DB
            $this->products->icon = $filename;
       }

        if($request->hasFile('images')) {
            // get each image
            $imgs = ["img1","img2","img3"];
            $images = $request->file('images');
            $i=1;
            foreach($images as $image) {
            	$extension = $image->getClientOriginalExtension();

            	// check if extension is .exe or .dll 
            	if($extension == 'exe' || $extension == 'dll') {
            		return redirect()->back()->withInput()->withWarning('That is not a valid image file! Please choose another one.');
            	}

            	// continue if correct
                $filename  = $nicename . '-screenshot' . $i . '-' . time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('/backend/img/' . $this->get_the_category_detail($category, 'folder') . '/' . $filename); //location to save
                Image::make($image->getRealPath())->save($path);

                // save into DB
                $img = $imgs[($i-1)];
                $this->products->$img = $filename; // convert to json then save
                $i++; // increment i
            }

        }

        // finally save everything
        $row = $this->products->save();
        
        if($row) {
            return redirect()->back()
                ->withSuccess('Successfully Added the App');
        }
        return redirect()->back()
            ->withInput()
            ->withFail('Error Adding the Product');


    }

    /**
     * view all resources settings.
     *
     * @return view
     */
    public function apps()
    {
        return view('admin.apps')
            ->withTitle('All Apps')
            ->withIcon('android')
            ->withPage_desc('View all uploaded apps')
            ->withPage('Apps')
            ->withProducts($this->products->orderBy('created_at', 'DESC')->paginate(30));

    }
    
    
    /**
     * Present a form for allocating a new resources.
     *
     * @return view
     */
    public function create()
    {
        return view('admin.new')
            ->withTitle('Add an App')
            ->withIcon('android')
            ->withPage_desc('Add a new app to the list')
            ->withPage('New App')
            ->withNav_items($this->nav_items)
            ->withCategories($this->categories->get());
    }

    
     /**
     * edit a resource.
     *
     * @return view
     */
    public function edit($id)
    {
        $the_product =  $this->products->find($id);

        return view('admin.edit')
            ->withTitle('Edit ' . $the_product->appname)
            ->withIcon('android')
            ->withPage_desc('View all uploaded apps')
            ->withPage('Apps')
            ->withProduct($the_product)
            ->withNav_items($this->nav_items)
            ->withCategories($this->categories->get());
    }
    
     
    /**
     * update a resource.
     *
     * @return view
     */
    public function update(Request $request)
    {
        // dd($request);
        $id = $request->get('product-id');
        $the_product =  $this->products->find($id);

        // initialize variables 
        $screenshots = [];
        $screenshots_to_remove = false;
        $final_screenshots = [];
        $i=0;

        // collect request
        $appname = $request->get('name');
        $category = $request->get('category');
        $device_type = $request->get('device_type');
        $description = $request->get('description');
        $nicename = str_replace(" ", "", $appname);

        if($request->has('removeScreenshots')) {
            $screenshots_to_remove = $request->get('removeScreenshots');
        }

        // dd($screenshots_to_remove);
        // first delete the images 
        if($screenshots_to_remove) {
            foreach($screenshots_to_remove as $a_screenshot) {
                $the_product->$a_screenshot = '';
                $the_product->save();
            }
        }
        

        // start saving info
        $the_product->user_id = Auth::user()->id;
        $the_product->appname = $appname;
        $the_product->category_id = $category;
        $the_product->device_type = $device_type;
        $the_product->description = $description;

        //  check if file needs an update (saving the file apk)
        if($request->hasFile('app')) {
            // get file 
            $app = $request->app->isValid();
            // $extension ='ext';
            $extension = $request->file('app')->extension();
            $extension_to_save = $request->file('app')->getClientOriginalExtension();

            // dd($extension . ' ex ' . $extension_to_save);
            // filename
            $filename = $nicename . time() . '.' . $extension; // filename

            // $path = $request->app->storeAs('/public', $filename); // storepath, filename
            $path = public_path(). '/backend/apps/'; // destination path
            $request->app->move($path, $filename); // move

            // save into DB 
            $the_product->filename = $filename;
            $the_product->extension = $extension_to_save;
            
        } else {
            echo 'no app';
        }

        // check if icon needs an update 
        if($request->hasFile('icon')) 
        {
            $image = $request->file('icon');
            $filename  = $nicename . '-icon-' . time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('/backend/img/students/' . $filename);
    
            $path = public_path('/backend/img/' . $this->get_the_category_detail($category, 'folder') . '/' . $filename); //location to save
            Image::make($image->getRealPath())->save($path);

            // save into DB
            $the_product->icon = $filename;
       }

        // check if the screenshots need an update
        $imgs = ["img1","img2","img3"];
        $i=0;
        foreach($imgs as $img) {
            if($request->hasFile($img)) {
                // get each image
                $the_img = $request->file($img);
                $filename  = $nicename . '-screenshot' . ($i + 1) . '-' . time() . '.' . $the_img->getClientOriginalExtension();

                $path = public_path('/backend/img/' . $this->get_the_category_detail($category, 'folder') . '/' . $filename); //location to save
                Image::make($the_img->getRealPath())->save($path);

                // save into DB
                $the_product->$img = $filename; 
            }
            $i++;
        }

        // save the whole damn thing 
        $row = $the_product->save();

        if($row) {
            return redirect()->route('admin.apps')
                ->withSuccess('Successfully Updated the App');
        }
        return redirect()->back()
            ->withInput()
            ->withFail('Error Updating the Product');        

    }

    /**
     * Remove entries of a single resources.
     *
     * @return view
     */
    public function destroy($id)
    {
        $the_product = $this->products->find($id);
        // first delete the resources, i.e the icon, screenshots, and appfile 
        // delete icon
        $icon = $the_product->icon;
        File::delete(public_path() . '/backend/img/' . $this->get_the_category_detail($the_product->category_id, 'folder') . '/' . $icon); 

        // delete screenshots
        File::delete(public_path() . '/backend/img/' . 
            $this->get_the_category_detail($the_product->category_id, 'folder') . '/' . $the_product->img1); 
        File::delete(public_path() . '/backend/img/' . $this->get_the_category_detail($the_product->category_id, 'folder') . '/' . $the_product->img2); 
        File::delete(public_path() . '/backend/img/' . $this->get_the_category_detail($the_product->category_id, 'folder') . '/' . $the_product->img3); 

        // delete app file
        File::delete(public_path() . '/backend/apps/' . $the_product->filename);

        //  now delete the DB record
        $row = $the_product->delete();

        if($row) {
            return redirect()->back()
                ->withSuccess('Successfully deleted the App');
        }
        return redirect()->back()
            ->withFail('An Error was Encountered While Deleting the App');
    }

    /**
     * view all resources for deleting.
     *
     * @return view
     */
    public function delete(Request $request)
    {
        $filters = [];

        // fetch get parameters
        if($request->has('filters')) {
            $start_date = $request->get('start-date');
            $end_date = $request->get('end-date');

            $filters = [
                'start_date'=>Carbon::parse($start_date)->format('j M, Y'), 
                'end_date'=>Carbon::parse($end_date)->format('j M, Y')
            ];
            $products = $this->products->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->paginate(100);
        } else {
            // else if no action is provided, fetch 50 records
            $products = $this->products->orderBy('created_at', 'DESC')->paginate(50);
        }

        return view('admin.delete')
            ->withTitle('Delete Apps in Bulk Method 1')
            ->withIcon('trash')
            ->withPage_desc('Delete Apps in Bulk Method 1')
            ->withPage('Delete')
            ->withFilters($filters)
            ->withProducts($products);

    }

}
