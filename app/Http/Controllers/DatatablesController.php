<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// models 
use App\Product; // the product
use App\Category; // all categories
use App\User; // all users

// datatables
use Datatables; // datatable

class DatatablesController extends Controller
{
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function productsData()
    {
        // $app = Product::select('products.*')
        //             ->with('category')
        //             ->get();
        return Datatables::of(Product::query())
                        ->addColumn('appicon', function($data) {
                            return strtolower($data->category->folder) . '/' . $data->icon;
                        })
                        ->make(true);

        // return Datatables::of($app)->make(true);
        // return Datatables::of(Product::query())->make(true);
    }

    /**
     * =======================================================
     * Process datatables ajax request.
     * 
     * Get the basic implementation of datatables from this example
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function studentData(Request $request)
    {
        // return Datatables::of(Student::query())->make(true);

        // return students who have not yet graduated ie status != 4
        // return Datatables::eloquent(Student::query())->where('status','!=',4)->make(true);
        // $student = Student::where('status', '!=', 4)->leftJoin('student_details', 'students.adm_no', '=', 'student_details.adm_no');
        $student = Student::select('students.*')
                    ->where('status', '!=', '4')
                    ->with('studentDetail')
                    ->get();
        return Datatables::of($student)->make(true);
    }
}
