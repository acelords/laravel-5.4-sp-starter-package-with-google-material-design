<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditSiteSettingsRequest; // edit site settings
use App\Http\Requests\AddAppRequest; // add app


use App\Product; // model
use App\SiteDetail; // model
use App\Category; // model
use Auth; // logged in user
use Image; // images
use File; // files
use Response; // responses
use Carbon\Carbon; // date formating

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->products = new Product();
        $this->categories = new Category();
        // $this->nav_items = array("android","blackberry","ios","html 5","java","symbian","windows phone","windows mobile");
        $this->nav_items = array("Android 4.4", "Andoid 5", "Android 5.1", "Android 6", "Android 7");
    }

    // fetch categories 
    protected function get_the_category_detail($cat_id, $field) {
        $all_cats = $this->categories->select(['id','folder'])->get();
        foreach($all_cats as $the_cat) {
            if($the_cat->id == $cat_id) {
                return $the_cat->$field;
            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // set up some things for demo purposes
        $previous_week = date('Y-m-d', strtotime('-1 week'));
        $downloads = collect([
            ['id'=>'1', 'created_at'=>date('Y-m-d', strtotime('-6 days'))],
            ['id'=>'2', 'created_at'=>date('Y-m-d', strtotime('-5 days'))],
            ['id'=>'3', 'created_at'=>date('Y-m-d', strtotime('-4 days'))],
            ['id'=>'4', 'created_at'=>date('Y-m-d', strtotime('-5 days'))],
            ['id'=>'5', 'created_at'=>date('Y-m-d', strtotime('-6 days'))],
            ['id'=>'6', 'created_at'=>date('Y-m-d', strtotime('-1 days'))],
        ]);
        // dd($downloads);

        return view('admin.index')
            ->withIcon('home')
            ->withTitle('dashboard')
            ->withPage('dashboard')
            ->withApps_count(Product::count())
            ->withCategories_count(Category::count())
            ->withDownloads_count(Product::sum('downloads'))
            ->withProducts($this->products->get())
            ->withNav_items($this->nav_items)
            ->withCategories($this->categories->get())
            // reporting
            ->withMost_downloaded_apps(Product::orderBy('downloads')->limit(5)->get());
            // ->withDownloads_this_week(Download::where('created_at', '>=', $previous_week)
                                // ->select(['id', 'created_at'])
                                // ->orderBy('created_at')
                                // ->get()
                                // ->groupBy(function($date){
                                //     return Carbon::parse($date->created_at)->format('d');
                                // })
            // )
            // ->withDownloads_this_week($downloads);

    }


    /**
     * present view for deleting resource individually.
     *
     * @return view
     */
    public function datatables()
    {
        // dd('done');
        return view('admin.datatables')
            ->withTitle('Datatables Example')
            ->withIcon('table')
            ->withPage_desc('Datatables Example for working with Mega Records')
            ->withPage('Datatables')
            ->withNav_items($this->nav_items)
            ->withCategories($this->categories->get());
    }


    /**
     * view site settings.
     *
     * @return view
     */
    public function settings()
    {
        return view('admin.settings')
            ->withTitle('Settings')
            ->withIcon('cogs')
            ->withPage_desc('View all details pertaining to the site')
            ->withPage('Settings');
    }

     /**
     * Edit site settings.
     *
     * @return \Illuminate\Http\Response
     * @return view
     */
    public function settingsUpdate(EditSiteSettingsRequest $request, SiteDetail $setting)
    {
        $row = $setting->where('option_name', 'site_name')->update(['option_value' => $request->get('site_name')]);
        $row = $setting->where('option_name', 'site_url')->update(['option_value' => $request->get('site_url')]);
        $row = $setting->where('option_name', 'site_slogan')->update(['option_value' => $request->get('site_slogan')]);
        $row = $setting->where('option_name', 'site_desc')->update(['option_value' => $request->get('site_desc')]);
        $row = $setting->where('option_name', 'site_bio')->update(['option_value' => $request->get('site_bio')]);
        $row = $setting->where('option_name', 'site_address')->update(['option_value' => $request->get('site_address')]);
        $row = $setting->where('option_name', 'site_phone')->update(['option_value' => $request->get('site_phone')]);
        $row = $setting->where('option_name', 'site_email')->update(['option_value' => $request->get('site_email')]);
        $row = $setting->where('option_name', 'site_fax')->update(['option_value' => $request->get('site_fax')]);

        // social networks
        $row = $setting->where('option_name', 'site_facebook')->update(['option_value' => $request->get('site_facebook')]);
        $row = $setting->where('option_name', 'site_twitter')->update(['option_value' => $request->get('site_twitter')]);
        $row = $setting->where('option_name', 'site_instagram')->update(['option_value' => $request->get('site_instagram')]);
        $row = $setting->where('option_name', 'site_googleplus')->update(['option_value' => $request->get('site_googleplus')]);
        $row = $setting->where('option_name', 'site_youtube')->update(['option_value' => $request->get('site_youtube')]);

        if ($row) {
            return redirect()->route('admin.settings')
                ->withSuccess('Settings Updated!');
        }
        return redirect()->route('admin.settings')
            ->withFail('Error in saving Settings!');
    }

    /**
     * view site main logo.
     *
     * @return view
     */
    public function mainLogo()
    {
        $main_logo = SiteDetail::where('option_name', 'site_logo')->first();
        $path = public_path() . '/img/' . $main_logo->option_value;

        $element = '<img class="img-responsive" title="Main Logo" src="' . $path . '" />';

        return $main_logo->option_value;
    }

    /**
     * update site main logo.
     *
     * @return view
     */
    public function updateLogo(SiteDetail $site_detail, Request $request)
    {
        // dd($request);
        $main_logo = $request->get('file');

        $storeFolder = public_path(); //2 

        $ext = $request->file('file')->getClientOriginalExtension(); //to get the file extension
        // $original_name = $request->file('file')->getClientOriginalName(); //to get the file name

        $filename = 'main-logo-' . time() . '.' . $ext;

        if($request->hasFile('file')) { //check if indeed there are files uploaded
            // $tempFile = $_FILES['file']['tmp_name'];    //3
            $targetPath = $storeFolder . '/img/';   //4
            
            // $targetFile = $targetPath . $_FILES['file']['name']; //5
            $targetFile = $targetPath . $filename;  // new filename saved

            // move_uploaded_file($tempFile, $targetFile); //6  
            $upload_success = $request->file->move($targetPath, $filename); // move

            // delete previous logo
            // $deleteFile = $targetPath.$old['avatar'];

            // update main logo
            $the_site_logo = $site_detail->where('option_name', '=', 'site_logo')->first();
            $the_site_logo->option_value = $filename;
            $row = $the_site_logo->save();

            if($row) {
                return Response::json('success', 200);
            } else {
                return Response::json('error', 400);
            }
        } 

        return ;
    }

}
