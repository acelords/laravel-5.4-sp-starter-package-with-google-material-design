<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; // Schema

use App\SiteDetail; //the model

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // make laravel default to default DB char set on servers
        // running on MySQL <v5.7.7 for migration sake
        Schema::defaultStringLength(191);

        /**
        *   Set up Site Details to be shared to all views
        *
        */
        $s = SiteDetail::all();
        // dd($s[10]['option_value']);

        // get option. Essential in that it returns an empty string 
        // in case the option value/name does not exist to avoid errors
        function get_option($s, $str) {
            if(count($s) > 0) {
                foreach( $s as $s_detail) {
                    if($s_detail->option_name == $str) {
                        return $s_detail->option_value;
                    }
                }
            } else {
                return '';
            }
        }

        // foreach($s as $s_detail) {
        //     echo get_option($s, "site_name");
        //     echo '<br>';
        //     echo get_option($s, "site_facebook");
        // }
        // dd('done');
        view()->share([
            'site_name'=>get_option($s, "site_name"), 
            'site_url'=>get_option($s, "site_url"), 
            'site_slogan'=>get_option($s, "site_slogan"),
            'site_desc'=>get_option($s, "site_desc"),
            'site_bio'=>get_option($s, "site_bio"),
            'site_address'=>get_option($s, "site_address"),
            'site_phone'=>get_option($s, "site_phone"),
            'site_email'=>get_option($s, "site_email"),
            'site_fax'=>get_option($s, "site_fax"),
            'site_logo'=>get_option($s, "site_logo"),
            'site_facebook'=>get_option($s, "site_facebook"),
            'site_twitter'=>get_option($s, "site_twitter"),
            'site_googleplus'=>get_option($s, "site_googleplus"),
            'site_youtube'=>get_option($s, "site_youtube"),
            'site_instagram'=>get_option($s, "site_instagram"),
            'site_version'=>get_option($s, "site_version")
        ]);
        // dd('end');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
