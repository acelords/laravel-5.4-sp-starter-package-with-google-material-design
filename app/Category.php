<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /** 
    *   Fetch Products under this category
    */
    public function products() {
        return $this->hasMany('App\Product');
    }
}
