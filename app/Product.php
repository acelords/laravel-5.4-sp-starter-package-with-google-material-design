<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'downloads', 'appname', 'description', 'device_type','icon','filename','img1','img2','img3'
    ];

    
    /** 
    *   Get Categories the product belongs to
    */
    public function category() {
        return $this->belongsTo('App\Category');
    }
}
