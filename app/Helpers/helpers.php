<?php

// for nav and active elements
function check_active($str, $resp) {
	if (strtolower($str) == Request::segment(2))
		echo ' ' . $resp .' ';
}

// good for returning string if condition is met, e.g select in forms
function check_if_same($str1, $str2, $value) {
	if (strtolower($str1) == strtolower($str2))
		echo $value;
}

// check if null, return condition accordingly (works with objects)
function check_if_null($string, $value_if_exists, $value_if_null = '') {
	if(is_null($string)) {
		return $value_if_null;
	} else {
		return $string->$value_if_exists;
	}
}

// ratings
function get_product_ratings($ratings=24, $raters=5) {
	// calculation: total-ratings / raters 
	$average_ratings = $ratings / $raters;
	if($average_ratings <= 3) {
		return '<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>';
	} else if($average_ratings > 3 && $average_ratings < 4) {
		return '<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star-half-empty"></i>';
	} else if($average_ratings == 4) {
		return '<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>';
	} else if($average_ratings >= 4 && $average_ratings < 5) {
		return '<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star-half-empty"></i>';
	} else {
		return '<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>
		<i class="fa fa-star"></i>';
	}
}